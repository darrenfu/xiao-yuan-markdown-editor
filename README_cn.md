# XiaoYuanMarkdownEditor

#### 介绍
是一款功能简单的MarkDown文件编辑器，可以实现Markdown文件的编辑和保存，该编辑器为教学案例，完全开源，大家可以根据自己的实际情况下载使用,详细专栏教程请参见：[专栏链接](https://blog.csdn.net/fwj380891124/category_11246398.html?spm=1001.2014.3001.5482)

#### 安装说明
1. 下载代码
2. 按教程全局安装好electron
3. 执行npm start查看效果 效果图： 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/150222_95f5bb8b_507577.png "show.png")